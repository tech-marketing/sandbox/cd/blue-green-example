#!/bin/bash
# replace 35.231.14.143.nip.io with the corresponding DNS name of your K8s cluster
echo -n "blue: "
curl --insecure https://blue-tech-marketing-sandbox-cd-blue-green-example.35.231.14.143.nip.io; echo
# echo -n "secure: "
# if curl https://blue-atroschinetz-amy-test.amytest.joaocunha.eu/ >/dev/null 2>&1; then
#     echo "yes"
# else
#     echo "no"
# fi
 echo -n "ingress spec.tls[].hosts: "
 kubectl -n production get ingresses blue-auto-deploy -o json | jq -r ".spec.tls[].hosts"
echo -n "live: "
blueIsLive=$(kubectl -n production get deploy blue -o json | jq -r '.metadata.annotations."bluegreen/live"')
echo "$blueIsLive"
echo $'\n---\n'
echo -n "green: "
curl --insecure https://green-tech-marketing-sandbox-cd-blue-green-example.35.231.14.143.nip.io; echo
# echo -n "secure: "
# if curl https://green-atroschinetz-amy-test.amytest.joaocunha.eu/ >/dev/null 2>&1; then
#     echo "yes"
# else
#     echo "no"
# fi
 echo -n "ingress spec.tls[].hosts: "
 kubectl -n production get ingresses green-auto-deploy -o json | jq -r ".spec.tls[].hosts"
echo -n "live: "
greenIsLive=$(kubectl -n production get deploy green -o json | jq -r '.metadata.annotations."bluegreen/live"')
echo "$greenIsLive"
echo $'\n---\n'
if [ "$blueIsLive" == "true" ]
then
  echo "production is pointing to blue"
else
  echo "production is pointing to green"
fi
echo -n "live: "
curl --insecure https://tech-marketing-sandbox-cd-blue-green-example.35.231.14.143.nip.io; echo
# echo -n "secure: "
# if curl https://atroschinetz-amy-test.amytest.joaocunha.eu/ >/dev/null 2>&1; then
#     echo "yes"
# else
#     echo "no"
# fi

