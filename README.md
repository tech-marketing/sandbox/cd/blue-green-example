# Blue-Green Example

This project demonstrates an example
[blue-green deployment](https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html#blue-green-deployment)
implementation for [Kubernetes on GitLab](https://docs.gitlab.com/ee/user/project/clusters/)
using [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

## Setup instructions

1. Create a K8s cluster via the GitLab K8s integration. If using GKE, use Zone us-east1-* and MachineType e2-standard2

1. On your K8s cluster, from the Applications tab, start applications: Ingress, Cert-Manager and Prometheus

1. On your K8s cluster, from the Advanced Settings tab, set the Cluster Management Project to be this project, e.g. tech-marketing/sandbox/cd/blue-green-example

1. On your K8s cluster, from the Details tab, set the Base domain. In GKE, you can use the IP address plus nip.io; in EKS, you will need to create a DNS name and map it as a CNAME to your long EKS URL

1. Clone the project to your local directory and ensure that the file check.sh is executable; e.g. chmod +x check.sh

1. Run the pipeline. The first stage will build the project and then wait for the next manual step.

1. Run job "deploy-to-blue" to deploy current application to blue environment (pod will be created in your K8s cluster), under namespace production

1. At this point, you have your blue deployment up and running

1. Run job "switch-to-blue" to have the production environment point to the blue pod

1. On the production Environment dashboard, click on Edit and set the field External URL to your production URL, e.g. https://tech-marketing-sandbox-cd-blue-green-example.35.231.14.143.nip.io

1. Edit your server.rb file in project and make a modification to the string in line 9; modify, response.body = 'Hello World!', to response.body = 'Hello Green World!'

1. Go back to pipeline and run job "deploy-to-green" to deploy current application to green environment (pod will be created in your K8s cluster), under namespace production

1. At this point, you have your green and blue deployment both up and running

1. Run job "switch-to-green" to have the production environment point to the green pod

1. You can run "switch-to-blue" or "switch-to-green" at any point in time from now on in any order you want

1. To see the output of the switch, you can either open the applications from the Environments page, OR run the check.sh script from your local laptop (you will need to update the URLs in the script to point to your K8s clusters)


## Demo

For more details on how to operate the configuration in this example, this
[video demonstration](https://www.youtube.com/watch?v=ymgwt2NUbd4) shows it in action.

## Caveats

When using this configuration, the `Open live environment` button does not show
up for some environments in your **Operations > Environments** page. This does
not necessarily mean that the environment is not live. You should be able to
access live environments by going to their URL directly.

You may also see the error `Kubernetes deployment not found` for the
`production` environment. This is because there are no Kubernetes resources
dedicated to that environment. Instead, it is the blue or green environments
themselves that will act as the production environment when they are switched to
being the active environment.

There is further documentation on drawbacks, considerations, and issues you may
encounter written inline in the
[`gitlab-ci.yml` file in this repository](.gitlab-ci.yml).
